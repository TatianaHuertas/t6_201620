package taller.mundo;

public class Ciudadano
{
	// Atributos
	/**
	 * Nombre del ciudadano
	 */
	private String nombre;
	
	/**
	 * Apellido del ciudadano
	 */
	private String apellido;
	
	/**
	 * Número del acudiente del ciudadano
	 */
	private String numero;
	
	/**
	 * Latitud del ciudadano
	 */
	private long latitud;
	
	/**
	 * Longitud del ciudadano
	 */
	private long longitud;
	
	// Constructor
	public Ciudadano(String nombre, String apellido, String numero, long latitud, long longitud) 
	{
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.numero = numero;
		this.latitud = latitud;
		this.longitud = longitud;
	}

	// Getters
	public String darNombre() 
	{
		return nombre;
	}

	public String darApellido() 
	{
		return apellido;
	}

	public String darNumero()
	{
		return numero;
	}

	public long darLatitud() 
	{
		return latitud;
	}
	
	public long darLongitud()
	{
		return longitud;
	}
}

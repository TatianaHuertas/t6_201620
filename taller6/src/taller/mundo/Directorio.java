package taller.mundo;

import taller.estructuras.ListaEncadenada;
import taller.estructuras.TablaHash;

public class Directorio
{
	// Atributos
	/**
	 * Tabla que contiene a los ciudadanos.
	 */
	private TablaHash< String , Ciudadano > ciudadanos;

	// Constructor
	/**
	 * Crea un nuevo directorio.
	 */
	public Directorio()
	{
		super();
		ciudadanos = new TablaHash<String, Ciudadano>();
	}

	// Métodos

	/**
	 * Agrega los datos a la tabla.
	 * @param pDatos Datos de la tabla.
	 */
	public void agregarDatos(String[] pDatos) 
	{
		int j = 0;
		Ciudadano ciudadano = new Ciudadano(pDatos[j], pDatos[j+1], pDatos[j+2], 0, 0);
		ciudadanos.put(pDatos[j+2], ciudadano);
	}

	/**
	 * Agrega un ciudadano a la lista.
	 * @param pNombreCiudadano Nombre del ciudadano a agregar. pNombreCiudadano != null && pNombreCiudadano != "".
	 * @param pApellidoCiudadano Apellido del ciudadano a agregar. pApellidoCiudadano != null && pApellidoCiudadano != "".
	 * @param pNumeroCiudadano Número del ciudadano a agregar. pNumeroCiudadano > 0;
	 * @param pLatitudCiudadano Latitud del ciudadano a agregar. pLatitudCiudadano > 0;
	 * @param pLongitudCiudadano Longitud del ciudadano a agregar. pLongitudCiudadano > 0;
	 * @return true si se pudo agregar el ciudadano, false si no se puede agregar.
	 */
	public boolean agregarCiudadano(String pNombreCiudadano,String pApellidoCiudadano, String pNumeroCiudadano, long pLatitudCiudadano, long pLongitudCiudadano) 
	{
		boolean agrega = false;
		Ciudadano buscado = buscarCiudadanoPorAcudiente(pNumeroCiudadano);
		if( buscado == null )
		{
			Ciudadano ciudadano = new Ciudadano(pNombreCiudadano, pApellidoCiudadano, pNumeroCiudadano, pLatitudCiudadano, pLongitudCiudadano);
			ciudadanos.put(pNumeroCiudadano, ciudadano);			
			agrega = true;
		}
		return agrega;
	}

	/**
	 * Busca el ciudadano por el número del acudiente.
	 * @param pNumeroAcudiente Número del acudiente para buscar el ciudadano.
	 * @return el ciudadano por el número del acudiente.
	 */
	public Ciudadano buscarCiudadanoPorAcudiente(String pNumeroAcudiente) 
	{
		Ciudadano ciudadano = ciudadanos.get(pNumeroAcudiente);
		return ciudadano;
	}

	/**
	 * Retorna si un ciudadano fue buscado por nombre o por apellido.
	 * @param nombreOApellido Nombre o apellido a buscar.
	 * @return 1 si fue buscado por nombre, -1 si fue buscado por apellido, 0 si no lo encontró
	 */
	public int buscarCiudadanoPorNombreOApellido(String nombreOApellido) 
	{
		int buscado = 0;
		Ciudadano ciudadanoNombre = buscarCiudadanoNombre(nombreOApellido);

		if ( ciudadanoNombre != null  )
		{
			buscado = 1;
		}	

		if ( buscado != 1 )
		{
			Ciudadano ciudadanoApellido = buscarCiudadanoApellido(nombreOApellido);
			if ( ciudadanoApellido != null  )
			{
				buscado = -1;
			}
		}		
		return buscado;
	}

	/**
	 * Retorna el ciudadano con el nombre dado.
	 * @param pNombre El nombre del ciudadano que se va a buscar.
	 * @return el ciudadano con el nombre dado.
	 */
	public Ciudadano buscarCiudadanoNombre(String pNombre) 
	{
		Ciudadano ciudadano = null;

		for (int i = 0; i < ciudadanos.tabla().length && ciudadano == null; i++) 
		{
			Ciudadano aux = ciudadanos.tabla()[i].getValor();
			if ( aux.darNombre().equals(pNombre) )
			{
				ciudadano = aux;
			}
		}		
		return ciudadano;
	}

	/**
	 * Retorna el ciudadano con el apellido dado.
	 * @param pApellido El apellido del ciudadano que se va a buscar.
	 * @return el ciudadano con el apellido dado.
	 */
	public Ciudadano buscarCiudadanoApellido(String pApellido) 
	{
		Ciudadano ciudadano = null;

		for (int i = 0; i < ciudadanos.tabla().length && ciudadano == null; i++) 
		{
			Ciudadano aux = ciudadanos.tabla()[i].getValor();
			if ( aux.darApellido().equals(pApellido) )
			{
				ciudadano = aux;
			}
		}		
		return ciudadano;
	}

	/**
	 * Retorna el ciudadano con la localización dada por parámetro.
	 * @param latitudCiudadano Latitud del ciudadano a buscar.
	 * @param longitudCiudadano Longitud del ciudadano a buscar.
	 * @return el ciudadano con la localización dada por parámetro.
	 */
	public Ciudadano buscarCiudadanoPorLocalizacion(long latitudCiudadano, long longitudCiudadano) 
	{
		Ciudadano ciudadano = null;

		for (int i = 0; i < ciudadanos.tabla().length && ciudadano == null; i++)
		{
			Ciudadano aux = ciudadanos.tabla()[i].getValor();
			if ( aux.darLatitud() == latitudCiudadano && aux.darLongitud() == longitudCiudadano )
			{
				ciudadano = aux;
			}
		}		
		return ciudadano;
	}

	public TablaHash<String, Ciudadano> darCiudadanos()
	{
		return ciudadanos;
	}
}

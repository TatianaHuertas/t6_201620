package taller.estructuras;

public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar

	public enum tiposDeColisiones
	{
		LINEAR_PROBING,
		SEPARATE_CHAINING,
	}

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	private ListaEncadenada<K,V>[] tabla;	
	//TODO: Agruegue la estructura que va a soportar la tabla.

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash()
	{
		this.factorCarga = 0;
		this.factorCargaMax = 1000000;
		this.count = 0;
		this.capacidad = 6000000;
		tabla = new ListaEncadenada[this.capacidad];
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax)
	{
		this.capacidad = capacidad;
		this.factorCarga = 0;
		this.count = 0;
		this.factorCargaMax = factorCargaMax;
		tabla = new ListaEncadenada[this.capacidad];
		//TODO: Inicialice la tabla con los valores dados por parametro
	}

	public void put(K llave, V valor)
	{
		int i;
		for (i = hash(llave); tabla[i] != null; i++) 
		{
			if (tabla[i].getLlave().equals(llave)) 
			{
				tabla[i].anadir(valor);
			}
		}
		ListaEncadenada<K, V> nodo = new ListaEncadenada<K, V>(llave,valor);
		tabla[i] = nodo;
		
		count++;
		factorCarga= (100*count)/capacidad;
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
	}

	public V get(K llave)
	{
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		V val = null;

		for (int i = hash(llave); tabla[i] != null; i ++) 
		{
			if (tabla[i].getLlave().equals(llave))
			{            	
				val = tabla[i].getValor() ;
			}	
		}
		return val;
	}

	public V delete(K llave)
	{
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		if (!contains(llave)) 
			return null;

		int i = hash(llave);
		while (!llave.equals(tabla[i].getLlave())) 
		{
			i ++;
		}
		tabla[i] =null;
		i ++;
		while (tabla[i].getLlave() != null)
		{
			K   keyToRehash = tabla[i].getLlave();
			V valToRehash = tabla[i].getValor();
			tabla[i] = null;
			count--;  
			put(keyToRehash, valToRehash);
			i ++;
		}

		count--;        

		if ( count <=  factorCargaMax / 8 && count > 0 )
			resize( (int) factorCargaMax / 2);

		assert check();

		return null;
	}

	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		return ( llave.hashCode() & 0x7fffffff ) % capacidad;
	}

	//TODO: Permita que la tabla sea dinamica
	
	public boolean contains ( K llave) 
	{
		return get(llave) != null;
	}
	private boolean check()
	{
		if ( capacidad < 2* count) 
		{
			System.err.println("Hash table size  tamañoMaximo = " +  capacidad + "; array size  tamañoActual = " +  count);
			return false;
		}

		for (int i = 0; i <  capacidad; i++) 
		{
			if (tabla[i].getLlave() == null)
				continue;
			else if (get((K) tabla[i].getLlave()) != tabla[i].getValor())
			{
				System.err.println("get[" + tabla[i].getLlave() + "] = " + get((K) tabla[i].getLlave()) + "; vals[i] = " + tabla[i].getValor());
				return false;
			}
		}
		return true;
	}

	private void resize(int capacidad) 
	{
		TablaHash<K, V> temp = new TablaHash<K, V>(capacidad, factorCargaMax );

		for (int i = 0; i <  factorCargaMax; i++) 
		{
			if (tabla[i].getLlave() != null) 
			{
				try
				{
					temp.put((K)tabla[i].getLlave(), (V)tabla[i].getValor());
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		}
		tabla = temp.tabla();
		capacidad = temp.capacidad;
	}

	public ListaEncadenada<K, V>[] tabla() 
	{
		return tabla;
	}
}
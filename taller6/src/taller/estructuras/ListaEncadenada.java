package taller.estructuras;

public class ListaEncadenada<K extends Comparable<K> ,V>
{
	static final int DEFAULT = 1000;

	// Atributos
	int tamano;
	int cap;
	K llave;
	V valor;
	V[] datos;


	// Constructor
	@SuppressWarnings("unchecked")
	public ListaEncadenada( K key, V value ) 
	{
		this(DEFAULT);
		llave = key;
		valor = value;
		datos = null;
	}
	
	public ListaEncadenada(int capacidad) 
	{ 
		cap = capacidad;
		tamano = 0; 
	}

	// Metodos
	/**
	 * Modifica el item en la posicion 
	 * @param pos
	 * @param item
	 */
	public void anadir(V element)
	{
		if( tamano == cap)
		{
			V[] nuevosDatos = (V[]) new Object[cap*2];
			cap = cap*2;

			for( int i = 0; i<tamano; i++){
				nuevosDatos[i] = datos[i];
			}
			datos = nuevosDatos;
		}
		datos[tamano] = element;
		tamano++;
	}

	public int tamano()
	{
		return tamano;
	}

	public V obtener(int i) throws Exception
	{
		if( i < 0 || i >= tamano)
		{
			throw new Exception("No existe esa posición");
		}
		return datos[i];
	}

	public boolean estaVacio() {
		return tamano == 0;
	}

	public void removerPorIndice(int ind) throws Exception{

		if( ind < 0 || ind >=tamano)
		{
			throw new Exception("No existe esa posición");
		}

		for( int i = ind+1; i <tamano; i++)
		{
			datos[i - 1] = datos[i];
		}
		tamano --;
	}

	public void establecer( int i, V element) throws Exception{

		if( i < 0 || i >=tamano)
		{
			throw new Exception("No existe esa posición");
		}
		datos[i] = element;
	}

	public void intercambiar( int a, int b) throws Exception{

		if( a < 0 || a >=tamano || b < 0 || b >=tamano)
		{
			throw new Exception("No existe esa posición");
		}

		V ele = obtener(a);
		establecer( a, obtener(b));
		establecer( b, ele);
	}

	public K getLlave()
	{
		return llave;
	}

	public V getValor() 
	{
		return valor;
	}
}

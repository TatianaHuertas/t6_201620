package taller.interfaz;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import taller.mundo.Ciudadano;
import taller.mundo.Directorio;

public class Main {

	private static String rutaEntrada = "./data/ciudadLondres.csv";

	public static void main(String[] args) {
		//Cargar registros
		System.out.println("Bienvenido al directorio de emergencias por colicsiones de la ciudad de Londres");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		Directorio t = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();

			//TODO: Inicialice el directorio t
			t = new Directorio();
			int i = 0;
			entrada = br.readLine();
			while (entrada != null){
				String[] datos = entrada.split(",");

				//TODO: Agrege los datos al directorio de emergencias
				//Recuerde revisar en el enunciado la estructura de la información
				t.agregarDatos(datos);

				++i;
				if (++i%500000 == 0)
					System.out.println(i+" entradas...");

				entrada = br.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Directorio de emergencias por colisiones de Londres v1.0");

		boolean seguir = true;

		while (seguir)
			try {
				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar ciudadano a la lista de emergencia");
				System.out.println("2: Buscar ciudadano por número de contacto del acudiante");
				System.out.println("3: Buscar ciudadano por apellido");
				System.out.println("4: Buscar ciudadano por su locaclización actual");
				System.out.println("Exit: Salir de la aplicación");

				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String in = br.readLine();

				switch (in) {
				case "1":

					System.out.println("Ingrese el primer nombre del ciudadano: ");
					String nombreCiudadano = br.readLine();

					if( nombreCiudadano.equals("") || nombreCiudadano.equals(" "))
					{
						System.out.println("Se debe ingresar el nombre del ciudadano.");
						System.out.println("----------------------------------------------------");						
						System.out.println("Ingrese el primer nombre del ciudadano: ");
						nombreCiudadano = br.readLine();
					}					

					String[] nombreComp = nombreCiudadano.split(" ");
					if ( nombreComp.length > 1 )
					{
						if( nombreComp[1] != null )
						{
							System.out.println("Se debe registrar al ciudadano con un solo nombre.");
							System.out.println("----------------------------------------------------");						
							break;
						}
					}

					System.out.println("Ingrese el apellido del ciudadano: ");
					String apellidoCiudadano = br.readLine();

					if( apellidoCiudadano.equals("") || apellidoCiudadano.equals(" "))
					{
						System.out.println("Se debe ingresar el apellido del ciudadano.");
						System.out.println("----------------------------------------------------");						
						System.out.println("Ingrese el apellido del ciudadano: ");
						apellidoCiudadano = br.readLine();
					}

					System.out.println("Ingrese el número del acudiente del ciudadano: ");
					String numeroCiudadano = br.readLine();

					if ( numeroCiudadano.equals("") || numeroCiudadano.equals(" ") )
					{
						System.out.println("Se debe ingresar el número del acudiente del ciudadano.");
						System.out.println("----------------------------------------------------");						
						System.out.println("Ingrese el número del acudiente del ciudadano: ");
						numeroCiudadano = br.readLine();
					}

					System.out.println("Ingrese la latitud del ciudadano: ");
					String sLatitudCiudadano = br.readLine();
					long latitudCiudadano = 0;
					if ( !sLatitudCiudadano.equals("") || !sLatitudCiudadano.equals(" ") )
					{
						latitudCiudadano = Long.parseLong(sLatitudCiudadano);
					}
					else
					{
						System.out.println("Se debe ingresar la latitud del ciudadano.");
						System.out.println("----------------------------------------------------");						
						System.out.println("Ingrese la latitud del ciudadano: ");
						sLatitudCiudadano = br.readLine();
						latitudCiudadano = Long.parseLong(sLatitudCiudadano);
					}

					System.out.println("Ingrese la longitud del ciudadano: ");
					String sLongitudCiudadano = br.readLine();
					long longitudCiudadano = 0;
					if ( !sLongitudCiudadano.equals("") || !sLatitudCiudadano.equals(" "))
					{
						longitudCiudadano = Long.parseLong(sLongitudCiudadano);
					}
					else
					{
						System.out.println("Se debe ingresar la longitud del ciudadano.");
						System.out.println("----------------------------------------------------");						
						System.out.println("Ingrese la longitud del ciudadano: ");
						sLatitudCiudadano = br.readLine();
						latitudCiudadano = Long.parseLong(sLatitudCiudadano);
					}

					boolean agrega = t.agregarCiudadano(nombreCiudadano, apellidoCiudadano, numeroCiudadano, latitudCiudadano, longitudCiudadano);
					if (agrega)
					{
						System.out.println("El ciudadano ha sido agregado correctamente.");
						System.out.println("----------------------------------------------------");						
					}
					else
					{
						System.out.println("El ciudadano no pudo ser agregado.");
						System.out.println("----------------------------------------------------");						
						break;
					}					
					//TODO: Implemente el requerimiento 1.
					//Agregar ciudadano a la lista de emergencia
					break;
				case "2":

					System.out.println("Ingrese el número del acudiente del ciudadano que quiere buscar: ");
					String numeroAcudiente = br.readLine();

					Ciudadano ciudadano = t.buscarCiudadanoPorAcudiente(numeroAcudiente);
					if ( ciudadano != null)
					{
						System.out.println("El ciudadano identificado con el número: " + numeroAcudiente + " es: " + "\n" + ciudadano.darNombre() + " " + ciudadano.darApellido() );
						System.out.println("---------------------------------------------------------------------------------------------------------");						
					}
					else
					{
						System.out.println("El ciudadano con el número: " + numeroAcudiente + " no ha sido encontrado.");
						System.out.println("---------------------------------------------------------");						
						break;
					}
					//TODO: Implemente el requerimiento 2
					//Buscar un ciudadano por el número de teléfono de su acudiente
					break;
				case "3":

					System.out.println( "Escriba el nombre o el apellido del ciudadano que desea buscar: " );
					String nombreOApellido = br.readLine();

					int valor = t.buscarCiudadanoPorNombreOApellido(nombreOApellido);
					if ( valor != 0 )
					{
						Ciudadano ciudadano1 = null;
						if ( valor == 1)
						{
							ciudadano1 = t.buscarCiudadanoNombre(nombreOApellido);
							if ( ciudadano1 != null )
							{
								System.out.println("El ciudadano identificado con el nombre: " + nombreOApellido + " es: " + "\n" + ciudadano1.darNombre() + " " + ciudadano1.darApellido() + " - " + ciudadano1.darNumero());
								System.out.println("--------------------------------------------------------------------------------");						
							}
							else
							{								
								System.out.println("El ciudadano con el nombre: " + nombreOApellido + " no ha sido encontrado.");
								System.out.println("--------------------------------------------------------");	
							}
						}
						else if ( valor == -1 )
						{
							ciudadano1 = t.buscarCiudadanoApellido(nombreOApellido);
							if ( ciudadano1 != null )
							{
								System.out.println("El ciudadano identificado con el apellido: " + nombreOApellido + " es: " + "\n" + ciudadano1.darNombre() + " " + ciudadano1.darApellido() + " - " + ciudadano1.darNumero());
								System.out.println("--------------------------------------------------------------------------------");						
							}
							else
							{
								System.out.println("El ciudadano con el nombre: " + nombreOApellido + " no ha sido encontrado.");
								System.out.println("--------------------------------------------------------");	
							}
						}
						else
						{
							System.out.println("El ciudadano con el nombre: " + nombreOApellido + " no ha sido encontrado.");
							System.out.println("--------------------------------------------------------");						
							break;
						}
					}
						//TODO: Implemente el requerimiento 3
						//Buscar ciudadano por apellido/nombre
						break;
					case "4":

						System.out.println("Ingrese la latitud del ciudadano: ");
						String sLatitudCiudadanoB = br.readLine();
						long latitudCiudadanoB = Long.parseLong(sLatitudCiudadanoB);

						System.out.println("Ingrese la longitud del ciudadano: ");
						String sLongitudCiudadanoB = br.readLine();
						long longitudCiudadanoB = Long.parseLong(sLongitudCiudadanoB);

						Ciudadano ciudadano2 = t.buscarCiudadanoPorLocalizacion(latitudCiudadanoB, longitudCiudadanoB);
						if ( ciudadano2 != null  )
						{
							System.out.println("El ciudadano que se encuentra en la localización especificada es: " + "\n" + ciudadano2.darNombre() + " " + ciudadano2.darApellido() + " - " + ciudadano2.darNumero());
							System.out.println("-----------------------------------------------------------------------------------------------------------------------");						
						}
						else
						{
							System.out.println("El ciudadano con la localización especificada no ha sido encontrado.");
							System.out.println("--------------------------------------------------------------");						
							break;
						}					
						//TODO: Implemente el requerimiento 4
						//Buscar ciudadano por su locaclización actual
						break;
					case "Exit":
						System.out.println("Cerrando directorio...");
						seguir = false;
						break;

					default:
						break;
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	}
